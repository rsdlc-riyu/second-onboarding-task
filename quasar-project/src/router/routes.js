// routes.js
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: 'IndexPage', component: () => import('pages/IndexPage.vue'), name: 'IndexPage' },
      { path: 'DataComponent', component: () => import('pages/DataComponent.vue'), name: 'DataComponent' },
      { path: 'TableComponent', component: () => import('pages/TableComponent.vue'), name: 'TableComponent' },
    ]
  },

  // Always leave this as the last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
];

export default routes;
